﻿using System;
using System.Threading;
using Tachyon.Common;

namespace Tachyon.Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("*** Tachyon Sender *** ");
            Console.WriteLine("Press CTRL C to exit...");
            Console.WriteLine();
            Console.WriteLine("=========================");
            Console.WriteLine("Host name  : {0}", TachyonConstants.HostName);
            Console.WriteLine("Queue name : {0}", TachyonConstants.QueueName);
            Console.WriteLine("=========================");
            Console.WriteLine();
            Console.WriteLine("Sending messages");

            Run();
        }

        /// <summary>
        /// Runs this instance.
        /// </summary>
        private static void Run()
        {
            try
            {
                using (MessageSender sender = new MessageSender())
                {
                    int counter = 0;
                    TimeSpan timeout = new TimeSpan(0, 0, 0, 30);

                    while (true)
                    {
                        counter++;

                        string message = counter + " Tachyon message!";
                        Console.WriteLine("Sent message {0}.  Waiting {1} seconds for a response.", counter, timeout.Seconds);

                        string response = sender.Send(message, timeout);
                        Console.WriteLine("Received response message {0}", response);

                        Thread.Sleep(TachyonConstants.MessageSendInterval);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }
    }
}

﻿using System;
using System.Text;
using Tachyon.Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Tachyon.Sender
{
    public class MessageSender : IDisposable
    {
        private IConnection _connection;
        private IModel _model;
        private QueueingBasicConsumer _consumer;
        private string _responseQueue;
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageSender"/> class.
        /// </summary>
        public MessageSender()
        {
            Initialise();
        }

        /// <summary>
        /// Sends the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="timeout">The timeout.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">message</exception>
        public string Send(string message, TimeSpan timeout)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException("message");
            }

            string correlationToken = Guid.NewGuid().ToString();
            IBasicProperties properties = _model.CreateBasicProperties();
            properties.ReplyTo = _responseQueue;
            properties.CorrelationId = correlationToken;

            byte[] messageBuffer = Encoding.UTF8.GetBytes(message);

            // Send message to the queue
            PublishMessageToQueue(properties, messageBuffer);

            // Wait for response with timeout
            return GetResponseWithTimeout(timeout, correlationToken);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        private void Initialise()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = TachyonConstants.HostName,
                UserName = ConnectionFactory.DefaultUser,
                Password = ConnectionFactory.DefaultPass
            };

            _connection = connectionFactory.CreateConnection();
            _model = _connection.CreateModel();

            CreateQueue(TachyonConstants.QueueName);

            // Create dynamic response queue
            _responseQueue = _model.QueueDeclare().QueueName;
            _consumer = new QueueingBasicConsumer(_model);
            _model.BasicConsume(_responseQueue, true, _consumer);
        }

        /// <summary>
        /// Publishes the message to queue.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <param name="messageBuffer">The message buffer.</param>
        private void PublishMessageToQueue(IBasicProperties properties, byte[] messageBuffer)
        {
            _model.BasicPublish(TachyonConstants.ExchangeName,  // Exchange name
                                TachyonConstants.QueueName,     // Routing key
                                properties,                     // Properties
                                messageBuffer);                 // Message body
        }

        /// <summary>
        /// Gets the response with timeout.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <param name="correlationToken">The correlation token.</param>
        /// <returns></returns>
        /// <exception cref="System.TimeoutException"></exception>
        private string GetResponseWithTimeout(TimeSpan timeout, string correlationToken)
        {
            DateTime timeoutAt = DateTime.Now.Add(timeout);

            while (DateTime.Now <= timeoutAt)
            {
                BasicDeliverEventArgs deliveryArgs;

                if (!_consumer.Queue.Dequeue(250, out deliveryArgs))
                {
                    continue; // Nothing retrieved from the response queue after waiting 250 milliseconds.
                }

                if (deliveryArgs?.BasicProperties != null && string.Equals(deliveryArgs.BasicProperties.CorrelationId, correlationToken, StringComparison.Ordinal))
                {
                    return Encoding.UTF8.GetString(deliveryArgs.Body);
                }
            }

            throw new TimeoutException(string.Format("No response received for ({0}).  Timeout ({1})", correlationToken, timeout));
        }

        /// <summary>
        /// Creates the queue.
        /// </summary>
        /// <param name="queueName">Name of the queue.</param>
        private void CreateQueue(string queueName)
        {
            _model.QueueDeclare(queueName,  // Queue name
                                false,      // Durable
                                false,      // Exclusive
                                false,      // Auto delete
                                null);      // Additional arguments
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///   <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _model?.Dispose();

                _connection?.Dispose();
            }

            _disposed = true;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="MessageSender" /> class.
        /// </summary>
        ~MessageSender()
        {
            Dispose(false);
        }

        #endregion
    }
}

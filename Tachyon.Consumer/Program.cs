﻿using System;
using Tachyon.Common;

namespace Tachyon.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("*** Tachyon Consumer *** ");
            Console.WriteLine("Press CTRL C to exit...");
            Console.WriteLine();
            Console.WriteLine("=========================");
            Console.WriteLine("Host name  : {0}", TachyonConstants.HostName);
            Console.WriteLine("Queue name : {0}", TachyonConstants.QueueName);
            Console.WriteLine("=========================");
            Console.WriteLine();
            Console.WriteLine("Waiting for messages");

            Run();
        }

        /// <summary>
        /// Runs this instance.
        /// </summary>
        private static void Run()
        {
            try
            {
                using (MessageConsumer consumer = new MessageConsumer { Enabled = true })
                {
                    consumer.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }
    }
}

﻿using System;
using System.Text;
using Tachyon.Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Tachyon.Consumer
{
    public class MessageConsumer : IDisposable
    {
        private IConnection _connection;
        private IModel _model;
        private bool _disposed;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MessageConsumer"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageConsumer"/> class.
        /// </summary>
        public MessageConsumer()
        {
            Initialise();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            QueueingBasicConsumer consumer = new QueueingBasicConsumer(_model);
            _model.BasicConsume(TachyonConstants.QueueName, false, consumer);

            while (Enabled)
            {
                // Get the next message from the queue
                BasicDeliverEventArgs deliverEventArgs = consumer.Queue.Dequeue();
                string message = Encoding.UTF8.GetString(deliverEventArgs.Body);
                Console.WriteLine("Message received ({0})", message);

                // Send response
                IBasicProperties properties = _model.CreateBasicProperties();
                properties.CorrelationId = deliverEventArgs.BasicProperties.CorrelationId;
                byte[] messageBuffer = Encoding.UTF8.GetBytes(string.Format("Processed message ({0})", message));
                _model.BasicPublish(TachyonConstants.ExchangeName, deliverEventArgs.BasicProperties.ReplyTo, properties, messageBuffer);

                // Acknowledge message is processed
                _model.BasicAck(deliverEventArgs.DeliveryTag, false);
            }
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        private void Initialise()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = TachyonConstants.HostName,
                UserName = ConnectionFactory.DefaultUser,
                Password = ConnectionFactory.DefaultPass
            };

            _connection = connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.BasicQos(0, 1, false);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///   <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _model?.Dispose();

                _connection?.Dispose();
            }

            _disposed = true;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="MessageConsumer" /> class.
        /// </summary>
        ~MessageConsumer()
        {
            Dispose(false);
        }

        #endregion
    }
}

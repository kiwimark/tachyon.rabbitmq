﻿namespace Tachyon.Common
{
    public static class TachyonConstants
    {
        /// <summary>
        /// The host name
        /// </summary>
        public const string HostName = "localhost";

        /// <summary>
        /// The name of the exchange to use in RabbitMQ
        /// </summary>
        public const string ExchangeName = "";

        /// <summary>
        /// The name of the RabbitMQ queue to send/receive messages
        /// </summary>
        public const string QueueName = "Tachyon_Queue";

        /// <summary>
        /// The interval to wait before sending another message to the queue
        /// </summary>
        public const int MessageSendInterval = 200;
    }
}

# README #

### What is this repository for? ###

Example of a sender to multiple consumers remote procedure call (RPC) implementation using Rabbit message queuing.


### How do I get set up? ###

1. Download and install Erlang (http://www.erlang.org/download.html)
1. Download and install RabbitMQ (https://www.rabbitmq.com/install-windows.html)
1. Open the RabbitMQ Management dashboard in a browser (Default http://localhost:15672/ Username:guest Password:guest)
1. Build Tachyon.RabbitMQ and run both a Sender and Consumer application
1. Watch the messages go..


### Who do I talk to? ###

Repo owner - kiwimark